import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from datetime import datetime
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score, recall_score,f1_score
import os.path


data = pd.read_csv('Fraud_Data.csv')
ip = pd.read_csv('IpAddress_to_Country.csv')


## Survey data

data.info()
data.isnull().sum()
ip.info()
ip.isnull().sum()

## Requirement 1: Determine her country based on the numeric IP address.

def get_ip(ip_code):
    if len(ip.loc[(ip.lower_bound_ip_address < ip_code) & (ip.upper_bound_ip_address>ip_code), 'country'])==0: # neu tim ko ra so để thỏa mãng trong dấu ngoặc để định vi index và suy ra country thì ra null và lengh sẻ bẳng 0
        return "None"
    else:
        aa=ip.loc[(ip.lower_bound_ip_address < ip_code) & (ip.upper_bound_ip_address>ip_code), 'country'].values[0]
        return aa

  
#### Test with IP=16777470
print(get_ip(16777470))# output: 'Australia'
  
## Requirement 2: Build a machine learning model that predicts the probability that the first transaction of a new user is fraudulent

if not os.path.isfile('data_full.csv'):
    ### calculate time between sign up and purchase
    data['signup_time'] = data['signup_time'].apply(pd.to_datetime).apply(lambda time: (time - np.datetime64('1970-01-01T00:00:00Z'))/np.timedelta64(1, 's'))
    data['purchase_time']= data['purchase_time'].apply(pd.to_datetime).apply(lambda time: (time - np.datetime64('1970-01-01T00:00:00Z'))/np.timedelta64(1, 's'))
    data['subtract_time'] = data['purchase_time'] - data['signup_time']
    ### them vao cot country tuong ung voi IP cho data
    data['country'] = data['ip_address'].apply(get_ip)
    data.to_csv('data_full.csv')
else:
    data = pd.read_csv('data_full.csv')

if not os.path.isfile('data_encode.csv'):    
    ### drop cols having string label and Convert string label to numeric label
    numeric_label = preprocessing.LabelEncoder()
    df = pd.DataFrame()
    df['sex'] = numeric_label.fit_transform(data['sex'])
    df['source'] = numeric_label.fit_transform(data['source'])
    df['subtract_time'] = numeric_label.fit_transform(data['subtract_time'])
    df['browser'] = numeric_label.fit_transform(data['browser'])
    df['country'] = numeric_label.fit_transform(data['country'])
    df['age']=data['age']
    df['purchase_value']=data['purchase_value']
    df.to_csv('data_encode.csv')
else:
    df = pd.read_csv('data_encode.csv')

print(df.head())    

x_data = df
y_data = data['class']

# xay dung mo hinh chon RandomForestClassifier de du doan
x_data_train, x_data_test, y_data_train, y_data_test = train_test_split(x_data, y_data, test_size=0.10)

# Create model RandomForest
model = RandomForestClassifier(n_estimators=60)
model.fit(x_data_train, y_data_train)

predictive_data = model.predict(x_data_test)


# boi vi day la du lieu imbalance data nen su dung f1_score de kiem tra do chinh xac
print(f1_score(y_data_test, predictive_data))
    



